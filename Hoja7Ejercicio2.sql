﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 1
*/

  
  /* 
    Crear la base de datos y seleccionarla
  */

  DROP DATABASE IF EXISTS hoja7unidad1modulo3Ejercicio2;
  CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3Ejercicio2;
  USE hoja7unidad1modulo3Ejercicio2;

/*
  crear las tablas 
 */

  -- compañia
  DROP TABLE IF EXISTS compañia;

  CREATE TABLE IF NOT EXISTS compañia(
    numero int,
    actividad varchar(50),
    PRIMARY KEY(numero)
    );


  -- soldado
  DROP TABLE IF EXISTS soldado;

  CREATE TABLE IF NOT EXISTS soldado(
    s int,
    nombre varchar(50),
    apellidos varchar(50),
    grado char(1),
    PRIMARY KEY(s)
    );
  
  -- servicio
  DROP TABLE IF EXISTS servicio;

  CREATE TABLE IF NOT EXISTS servicio(
    se int,
    descripcion varchar(100),
    PRIMARY KEY(se)
    );

  -- cuartel
  DROP TABLE IF EXISTS cuartel;

  CREATE TABLE IF NOT EXISTS cuartel(
    cu int,
    nombre varchar(100),
    direccion varchar(100),
    PRIMARY KEY(cu)
    );

  -- cuerpo
  DROP TABLE IF EXISTS cuerpo;

  CREATE TABLE IF NOT EXISTS cuerpo(
    c int,
    denominacion varchar(100),
    PRIMARY KEY(c)
    );

  -- pertenece compañia
  DROP TABLE IF EXISTS perteneceCompañia;

  CREATE TABLE IF NOT EXISTS perteneceCompañia(
    compañia int,
    soldado int,
    PRIMARY KEY (compañia,soldado),
    CONSTRAINT FKPerteneceCompañia FOREIGN KEY (compañia)
      REFERENCES compañia(numero) ON DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKPerteneceCompañiaSoldado FOREIGN KEY (soldado)
      REFERENCES soldado(s) on DELETE CASCADE on UPDATE CASCADE
    );

  -- realiza
  DROP TABLE IF EXISTS realiza;

  CREATE TABLE IF NOT EXISTS realiza(
    soldado int,
    servicio int,
    fecha date,
    PRIMARY KEY(soldado,servicio),
    CONSTRAINT FKRealizaSoldado FOREIGN KEY (soldado)
      REFERENCES soldado(s) ON DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKRealizaServicio FOREIGN KEY (servicio)
      REFERENCES servicio(se) ON DELETE CASCADE on UPDATE CASCADE
    );

  -- esta
  DROP TABLE IF EXISTS esta;

  CREATE TABLE IF NOT EXISTS esta(
    soldado int,
    cuartel int,
    PRIMARY KEY(soldado,cuartel),
    CONSTRAINT FKEstaSoldado FOREIGN KEY (soldado)
      REFERENCES soldado(s) ON DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKEstaCuartel FOREIGN KEY (cuartel)
      REFERENCES cuartel(cu) ON DELETE CASCADE on UPDATE CASCADE
    );
  
  -- pertenece cuerpo
  DROP TABLE IF EXISTS perteneceCuerpo;
  
  CREATE TABLE IF NOT EXISTS perteneceCuerpo(
    cuerpo int,
    soldado int,
    PRIMARY KEY(cuerpo,soldado),
    CONSTRAINT FKPerteneceCuerpo FOREIGN KEY (cuerpo)
      REFERENCES cuerpo(c) ON DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKPerteneceSoldado FOREIGN KEY (soldado)
      REFERENCES soldado(s) ON DELETE CASCADE on UPDATE CASCADE
    );



